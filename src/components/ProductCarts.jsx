

/**Styles for cards */
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';

/** Imports-- */
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {searchProductsOnCategory,isInCart} from '../data/productsearch';
import {addToKart} from '../data/basket';


/** Styles voor material ui */

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      margin: 'auto',
      maxWidth: 500,
    },
    image: {
      width: 128,
      height: 128,
    },
    img: {
      margin: 'auto',
      display: 'block',
      maxWidth: '100%',
      maxHeight: '100%',
    },
  }));

export default () => {
    /**Material ui styles */
    const classes = useStyles();

    const  { search, error, loading,products, categorie} = useSelector(state=> state.product)
    const {empty} = useSelector(state => state.basket)
    const dispatch = useDispatch();
    const clickToBasket = prodId => e => {
        dispatch(isInCart(prodId))
        dispatch(addToKart(prodId))
    }
    console.log(products)
    return(<> 
       <Grid container spacing={3} 
   direction="row"
   justify="center"
   alignItems="center"
 >
      {products.map(prod => (
          <Grid item s={12} md={6} xl={4}>
      <Paper className={classes.paper}>
        <Grid container spacing={2}>
          <Grid item>
            <ButtonBase className={classes.image}>
              <img className={classes.img} alt="complex" src={prod.imgUrl} />
            </ButtonBase>
          </Grid>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography gutterBottom variant="subtitle1">
                {prod.title}
                </Typography>
                <Typography variant="body2" gutterBottom>
                  
                </Typography>
                <Typography variant="body2" color="textSecondary">
                  {prod.type}
                </Typography>
              </Grid>
              <Grid item>
                  {!prod.basket&&<button display="block"  id={prod.id}onClick={clickToBasket(prod.id)} >add to Basket</button>}
                  {prod.basket&&<button display="block" disabled id={prod.id}onClick={clickToBasket(prod.id)} >in your basket</button>}
              </Grid>
            </Grid>
            <Grid item>
              <Typography variant="subtitle1">€{prod.price}</Typography>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
      </Grid>))}
    </Grid>
     </>
    )
}