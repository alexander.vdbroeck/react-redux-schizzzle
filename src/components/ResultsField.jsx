import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

import Box from '@material-ui/core/Box'
import Alert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';

import { useSelector, useDispatch } from "react-redux";
import {searchProductsOnCategory,isInCart} from '../data/productsearch';
import {addToKart} from '../data/basket'

import Categories from './Categories'
import PopUp from './PupUpBasket'
import ProductCards from './ProductCarts';

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      '& > * + *': {
        marginTop: theme.spacing(2),
      },
    },
  }));


export default ()=>{

    const classes = useStyles();

    const  { search, error, loading,products, categorie} = useSelector(state=> state.product)
    const {empty} = useSelector(state => state.basket)
  
return( <>
            {/**error and loading stuff */}
            {loading&&<Box textAlign="center"><Box width="100%"><CircularProgress /><p>loading</p></Box></Box>}
            {error&& <Alert severity="error">{error}</Alert>}
           
            {/**it there are items in your shopping cart , it pops up*/}
            {!empty&&<PopUp position="sticky"/>}

            {/**it there are  category's */}
            {categorie[0]&&<Categories/>}

            {/**the search results:*/}
            {products.length > 0 &&<ProductCards/>}
   
</>)
}