import React from 'react';
import { useDispatch } from "react-redux";

import {searchProducts} from '../data/productsearch'
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Container from "@material-ui/core/Container";
import { spacing } from '@material-ui/system';
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button';
import {useField,useError} from "../hooks/formHooks";


export default ()=>{
    const { error, setError, setValue, ...field } = useField("", true);
console.log(error);v

const dispatch = useDispatch();
const sumbitHandler = (e) =>  {
    e.preventDefault();
    if(field.value != ""){
        dispatch(searchProducts(field.value));
        setValue("")
  
    }else{
        setError(true)
    }


}
    return(
<form onSubmit={sumbitHandler}>
    <Box  
    style={{ backgroundColor: '#F0F6FF', height: '100px' }}
    alignItems="center"
    justifyContent="center"
        display="flex"
        flexWrap="nowrap"
        flexDirection="row">
            <Box m={2}
             width="70%">{error&&<TextField  error   variant="filled" label="Filled" fullWidth id="outlined-search" {...field}    label="you need to type in something" type="search" variant="outlined" />}
                {!error&&<TextField    variant="filled" label="Filled" fullWidth id="outlined-search" {...field}    label="search your product" type="search" variant="outlined" />}
            </Box>
            <Box m={1}
            width="30%">
                      <label htmlFor="contained-button-file">
                            <Button  onClick={sumbitHandler} variant="contained" color="primary" component="span" >
                                    Zoeken
                            </Button>
                    </label>
                
            </Box> 
    </Box>
    </form>
  )
}