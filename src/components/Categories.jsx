
/**----Material ui----- */
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography';
import { spacing } from '@material-ui/system';

/**imports  */
import React from 'react';
import { useSelector, useDispatch } from "react-redux";
import {searchProductsOnCategory} from '../data/productsearch'; 

export default ()=>{
    const {categorie} = useSelector(state => state.product)
    const dispatch = useDispatch();
    console.log(categorie);
    
    const clickHandler = catid => e => {
        dispatch(searchProductsOnCategory(catid))
    }

    return (
        <><Box
        alignItems="flex-start"
    justifyContent="flex-start"
        display="flex"
        flexWrap="wrap"
        flexDirection="row"
        ><Box width="100%" textAlign="center"><Typography variant="subtitle1">Verfijn uw zoekopdracht door een categorie te kiezen</Typography></Box>
            {categorie.map(cat => (<Box p={0.5}><Button variant="outlined" display="block" id={cat.id}onClick={clickHandler(cat.id)}>{cat.name} ({cat.productCount})</Button></Box>))}
            </Box></>
    )
}