
/**Material ui----- */
import { makeStyles } from '@material-ui/core/styles';
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper'
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';

/** imports */

import React from 'react';
import { useSelector, useDispatch } from "react-redux";

import {handleBasketItem, removeItem} from "../data/basket";
import {backFromCart} from '../data/productsearch';

/** styles van material ui */

const useStyles = makeStyles((theme) => ({
  typography: {
    padding: theme.spacing(2),
  },
  table: {
    minWidth: 650,
  },
}));

export default  () =>{
    const {empty,loading,products,nrOfItems,totalPrice} = useSelector(state => state.basket)
    const dispatch = useDispatch();
   const basketHandler = (prodId,plusOrMinus,minus= false) => (e) =>{
     if(plusOrMinus){
       if(minus){
        dispatch(handleBasketItem(prodId,true))

       }else{
        dispatch(handleBasketItem(prodId))
       }
     }else{
       dispatch(removeItem(prodId))
      dispatch(backFromCart(prodId))
     }
               
            /** */   

               
   }
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
      <AppBar position="sticky">
          <Box
          display="flex"
          justifyContent="center">
          <div>
      <Button  aria-describedby={id} variant="contained" color="primary" onClick={handleClick}>
       to your shopping cart
      </Button>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >   <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Product Name</TableCell>
            <TableCell align="right">Nr of items</TableCell>
            <TableCell align="right">price/unit</TableCell>
            <TableCell align="right">totalprice</TableCell>
            <TableCell align="right">Remove</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {products.map((prod) => (
            <TableRow key={prod[0].id}>
              <TableCell component="th" scope="row">
                {prod[0].title}
              </TableCell>
              <TableCell align="right">{prod[0].total}</TableCell>
              <TableCell align="right">{prod[0].price}</TableCell>
              <TableCell align="right">{prod[0].totalprice}</TableCell>
              <TableCell align="right"><button  id={prod[0].id}onClick={basketHandler(prod[0].id, true)}>+ 1</button> 
              {prod[0].total > 1 && <button  id={prod[0].id}onClick={basketHandler(prod[0].id,true,true)}>- 1</button> }         
              <button  id={prod[0].id}onClick={basketHandler(prod[0].id,false)}>remove</button></TableCell>
            </TableRow>
          ))}
                      <TableRow>
             <TableCell rowSpan={3} ></TableCell>
             <TableCell colSpan={2}>total price</TableCell>
          <TableCell align="right">{totalPrice}</TableCell>
           </TableRow>
           <TableRow>
             <TableCell  >Nr of items</TableCell>
             <TableCell align="right"></TableCell>
             <TableCell align="right">{nrOfItems}</TableCell>
           </TableRow>

        </TableBody>
      </Table>
    </TableContainer>

      </Popover>
    </div>
    </Box>
      </AppBar>
    
  );
}