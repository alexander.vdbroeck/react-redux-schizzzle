/**--------------------- initial state------------------ */
const init = {};
/**--------------------- Types-------------------------- */

/**--------------------- Action creator----------------- */

/**--------------------- Reducer------------------------ */
export default (state = init, { type, payload }) => {
  switch (type) {
    default:
      return state;
  }
};
