import axios from 'axios';

/**--------------------- initial state------------------ */
const init = {
	search: '',
	error: '',
	loading: false,
	products: [],
	categorie: []
};
/**--------------------- Types-------------------------- */
const START_PRODUCT_SEARCH = 'START_PRODUCT_SEARCH';
const SET_ERROR = 'SET_ERROR';
const SET_DATA = 'SET_DATA';
const SET_DATA_CAT = 'SET_DATA_CAT';
const START_CAT_SEARCH = 'START_CAT_SEARCH';
const SET_PROD_IN_BASKET = 'SET_PROD_IN_BASKET';
const PUT_ITEM_BACK_FROM_BASKET = 'PUT_ITEM_BACK_FROM_BASKET';
/**--------------------- Action creator----------------- */

/** Main actions */
/**http://localhost/phpapibol/index.php?search=test */
export const searchProducts = (string) => (dispatch) => {
	const searchString = string.replace(/[ ,]+/g, ',');
	dispatch(loadProducts(searchString));

	const searchUrl = 'https://www.byfelice.be/lol/index.php?search=' + searchString;
	axios
		.get(searchUrl)
		.then((result) => {
			if (result.data.products.length < 1) {
				dispatch(setError('Sorry there are no products'));
				console.log('no prod');
			} else {
				dispatch(setProductsAndCategory(result.data));
				console.log(result);
			}
		})
		.catch((e) => {
			dispatch(setError('Sorry someting went wrong'));
		});
};

export const searchProductsOnCategory = (catid) => (dispatch, useState) => {
	dispatch(loadProductsOnCategory());

	/** first find the search value in the state */
	const { product: { search } } = useState();

	const searchUrl = 'https://www.byfelice.be/lol/index.php?search=' + search + '&cat=' + catid;

	axios
		.get(searchUrl)
		.then((result) => {
			dispatch(setProductsNoCategory(result.data));
		})
		.catch((e) => {
			dispatch(setError('Sorry someting went wrong'));
		});
};

export const backFromCart = (prodId) => (dispatch) => {
	dispatch(itemBackFromCart(prodId));
};

/** filter for
 * the item that is put in
 * the basket and remove it
 */
export const isInCart = (prodId) => (dispatch) => {
	dispatch(putProductInBasket(prodId));
};

/** Sub actions */
/**Loading */

const loadProducts = (searchstring) => ({
	type: START_PRODUCT_SEARCH,
	payload: searchstring
});

const loadProductsOnCategory = () => ({
	type: START_CAT_SEARCH
});

/**Setting data */

const setProductsAndCategory = (payload) => ({
	type: SET_DATA,
	payload
});

const setProductsNoCategory = (payload) => ({
	type: SET_DATA_CAT,
	payload
});

const putProductInBasket = (prodId) => ({
	type: SET_PROD_IN_BASKET,
	payload: prodId
});
const itemBackFromCart = (prodId) => ({
	type: PUT_ITEM_BACK_FROM_BASKET,
	payload: prodId
});

/** error handling */

const setError = (err) => ({
	type: SET_ERROR,
	payload: err
});

/**--------------------- Reducer------------------------ */
export default (state = init, { type, payload }) => {
	switch (type) {
		case START_PRODUCT_SEARCH:
			return {
				...state,
				loading: true,
				search: payload,
				products: [],
				categorie: []
			};
		case SET_ERROR:
			return {
				...state,
				error: payload,
				loading: false,
				categorie: []
			};
		case SET_DATA:
			return {
				...state,
				loading: false,
				error: false,
				products: payload.products,
				categorie: payload.category
			};
		case START_CAT_SEARCH:
			return {
				...state,
				loading: true,
				products: [],
				categorie: []
			};
		case SET_DATA_CAT:
			return {
				...state,
				error: false,
				loading: false,
				products: payload.products,
				categorie: []
			};
		case SET_PROD_IN_BASKET:
			return {
				...state,
				products: state.products.map((item) => {
					if (item.id == payload) {
						item.basket = true;
						return item;
					} else {
						return item;
					}
				})
			};
		case PUT_ITEM_BACK_FROM_BASKET:
			return {
				...state,
				products: state.products.map((item) => {
					if (item.id == payload) {
						item.basket = false;
						return item;
					} else {
						return item;
					}
				})
			};
		default:
			return state;
	}
};
