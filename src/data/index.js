import { createStore, combineReducers, applyMiddleware } from "redux";
import productReducer from "./productsearch";
import basketReducer from "./basket";
import thunk from "redux-thunk";
import logger from "redux-logger";

export default createStore(
  combineReducers({ product: productReducer, basket: basketReducer }),
  applyMiddleware(thunk, logger)
);
