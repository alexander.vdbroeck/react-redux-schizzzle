import axios from "axios";

/**--------------------- initial state------------------ */
const init = {
  error: "",
  loading: false,
  products: [],
  empty: true,
  nrOfItems: 0,
  totalPrice: 0,
};
/**--------------------- Types-------------------------- */
const LOAD_ITEM = "LOAD_ITEM";
const ADD_EXTRA = "ADD_EXTRA";
const ADD_ITEM = "ADD_ITEM";
const SET_ERROR = "SET_ERROR";
const UPDATE_CART = "UPDATE_CART";
const UPDATE_PRICE = "UPDATE_PRICE";
const MIN_EXTRA = "MIN_EXTRA"

/**--------------------- Action creator----------------- */
/**Main actions */
export const addToKart = (prodId) => (dispatch, useState) => {
  dispatch(loadItem());
  axios
    .get("https://www.byfelice.be/lol/basket.php?cat=" + prodId)
    .then((result) => {
      dispatch(insertItem(result.data));
      dispatch(updatePriceAndTotal())
    })
    .catch((e) => {
      dispatch(setError("Sorry someting went wrong"));
    });
};



export const removeItem = (prodId) => (dispatch, useState) => {

  dispatch(updateCart(prodId));

  dispatch(updatePriceAndTotal());
};

export const handleBasketItem = (prodId, remove = false) => (dispatch) => {
  if (remove) {
    dispatch(removeExtraDispatch(prodId))
    dispatch(updatePriceAndTotal())

  } else {
    dispatch(addExtraDispatch(prodId))
  }

}

/**sub actions */

const loadItem = () => ({
  type: LOAD_ITEM
});
const insertItem = (item) => ({
  type: ADD_ITEM,
  payload: item
});
const updateCart = (prodId) => ({
  type: UPDATE_CART,
  payload: prodId
});
const updatePriceAndTotal = () => ({
  type: UPDATE_PRICE
});
const addExtraDispatch = (items) => ({
  type: ADD_EXTRA,
  payload: items
});

const removeExtraDispatch = (prodId) => ({
  type: MIN_EXTRA,
  payload: prodId
})




/** error handling */

const setError = (err) => ({
  type: SET_ERROR,
  payload: err
});
/**--------------------- Reducer------------------------ */
export default (state = init, {
  type,
  payload
}) => {
  switch (type) {
    case LOAD_ITEM:
      return {
        ...state, loading: true
      };
    case ADD_ITEM:
      return {
        ...state,
        loading: false,
          empty: false,
          products: [...state.products, payload.products],
      };
    case UPDATE_CART:
      return {
        ...state, error: false, loading: false, products: state.products.filter((prod) => {
          return prod[0].id != payload;
        })
      };
    case UPDATE_PRICE:
      return {
        ...state,
        empty: state.products.length > 0 ? false : true,
          /**  totalPrice: state.products.reduce((a, b) => a + b[0].price, 0),*/
          totalPrice: Number(Math.round(state.products.reduce((a, b) => a + b[0].totalprice, 0) + "e2") + "e-2"),
          nrOfItems: state.products.reduce((a, b) => a + b[0].total, 0),

      };
    case ADD_EXTRA:
      return {
        ...state,
        error: false,
          products: state.products.map(item => {
            if (item[0].id == payload) {
              item[0].total += 1;
              item[0].totalprice = Number(Math.round((item[0].total * item[0].price) + "e2") + "e-2")
              return item;
            } else {
              return item
            }
          }),
          totalPrice: Number(Math.round(state.products.reduce((a, b) => a + b[0].totalprice, 0) + "e2") + "e-2"),
          nrOfItems: state.products.reduce((a, b) => a + b[0].total, 0),
      };
    case MIN_EXTRA:
      return {
        ...state,
        error: false,
          products: state.products.map(item => {
            if (item[0].id == payload) {
              item[0].total -= 1;
              item[0].totalprice = Number(Math.round((item[0].total * item[0].price) + "e2") + "e-2")
              return item;
            } else {
              return item
            }
          }),

      };
    case SET_ERROR:
      return {
        ...state, loading: false, error: payload
      };
    default:
      return state;
  }
};