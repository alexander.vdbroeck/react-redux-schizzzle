import React from "react";
import ReactDOM from "react-dom";
import "./styles.css";
import * as serviceWorker from "./serviceWorker";
import Container from "./container";

ReactDOM.render(
  <React.StrictMode>
    <Container />
  </React.StrictMode>,
  document.getElementById("root")
);

serviceWorker.unregister();
