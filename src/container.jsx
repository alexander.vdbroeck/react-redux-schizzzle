import React from "react";
import { Provider } from "react-redux";

import Header from "./components/Header";
import SearchBar from './components/SearchBar';
import ResultsField from './components/ResultsField';

import store from './data/index';

import Container from "@material-ui/core/Container";

export default (props) => {
    return(
    <Provider store={store}>
    <Container maxWidth="md">
    <Header/>
    <SearchBar/>
    <ResultsField/>
    </Container>

    </Provider>)
    
    
}